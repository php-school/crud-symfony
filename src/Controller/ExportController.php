<?php

namespace App\Controller;

use App\Entity\User;
use Endroid\QrCode\QrCode;
use Knp\Snappy\Pdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExportController.
 */
class ExportController extends AbstractController
{
    /**
     * @Route("/xls", name="xls_hello_world")
     */
    public function xlsHelloWorld()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');

        $writer = new Xlsx($spreadsheet);
        $writer->save(__DIR__.'/../../public/xls/hello_world.xlsx');

        $response = new BinaryFileResponse(__DIR__.'/../../public/xls/hello_world.xlsx');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'hello_world.xlsx');

        return $response;
    }

    /**
     * @Route("/pdf", name="pdf_hello_world")
     */
    public function pdfGeneration(Pdf $generator)
    {
        $generator->generate('http://www.google.com', __DIR__.'/../../public/pdf/hello_world.pdf');

        $response = new BinaryFileResponse(__DIR__.'/../../public/pdf/hello_world.pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'hello_world.pdf');

        return $response;
    }

    /**
     * @Route("/qr", name="qr_code").
     */
    public function genratorQRCode()
    {
        $qrCode = new QrCode('https://andreybolonin.com/phpschool/');
        $qrCode->writeFile(__DIR__.'/../../public/qr/qrcode.png');

        $response = new BinaryFileResponse(__DIR__.'/../../public/qr/qrcode.png');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'qrcode.png');

        return $response;
    }

    /**
     * @Route("/download/user/xls", name="download_user_xls")
     */
    public function downloadUsers()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(User::class)->findAll();

        $excel = new Spreadsheet();
        $excel->setActiveSheetIndex(0);
        $sheet = $excel->getActiveSheet();

        $sheet->setTitle('Пользователи');

        $row = 3;
        $style = [
            'borders' => [
                'allborders' => [
                    'style' => Border::BORDER_THIN,
                ],
            ],
        ];

        /** @var User $user */
        foreach ($users as $user) {
            $sheet
                ->setCellValue('B1', 'Имя')
                ->setCellValue('B2', 'ID')
                ->setCellValue('C2', 'Позиція в лоті')
                ->setCellValue('D2', 'Наза лоту')
//                    ->setCellValue('E2', 'Порода')
//                    ->setCellValue('F2', 'Сорт')
//                    ->setCellValue('G2', 'Склад')
//                    ->setCellValue('H2', 'Діаметр')
//                    ->setCellValue('I2', 'Довжина')
//                    ->setCellValue('J2', 'Об\'єм')
//                    ->setCellValue('K2', 'Стартова ціна за м3')
//                    ->setCellValue('L2', 'Стартова вартість лоту')
//                    ->setCellValue('M2', 'Продавець')
//                    ->setCellValue('N2', 'Фактична ціна реалізації за м3')
//                    ->setCellValue('O2', 'Фактична вартість реалізації лоту')
//                    ->setCellValue('P2', 'Переможець по лоту')

                ->setCellValue('B'.$row, $user->getUsername())
                ->setCellValue('C'.$row, $user->getId())
//                    ->setCellValue('D'.$row, $lot->getName())
//                    ->setCellValue('E'.$row, $lot->getNomenclature()->getBreed())
//                    ->setCellValue('F'.$row, $lot->getNomenclature()->getSort())
//                    ->setCellValue('G'.$row, $lot->lotStorage())
//                    ->setCellValue('H'.$row, $lot->getNomenclature()->getDiameter())
//                    ->setCellValue('I'.$row, $lot->getLength())
//                    ->setCellValue('J'.$row, $lot->getVolume())
//                    ->setCellValue('K'.$row, $lot->getStartPriceM())
//                    ->setCellValue('L'.$row, $lot->getStartPriceVolume())
//                    ->setCellValue('M'.$row, $lot->getSeller()->getName())
//                    ->setCellValue('N'.$row, $lot->getActualPrice())
//                    ->setCellValue('O'.$row, ($lot->getStartPriceM() + $lot->getPrice()) * $lot->getVolume())
            ;
            ++$row;
        }
        --$row;
        $sheet->getStyle('B2:P'.$row)->applyFromArray($style);
        $objWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xls($excel);
        $filename = __DIR__.'/../../public/xls/users.xls';
        $objWriter->save($filename);

        return new BinaryFileResponse($filename);
    }
}
