```
git clone git@gitlab.com:php-school/crud-symfony.git
cd crud-symfony
composer install
bin/console doctrine:database:create
bin/console doctrine:schema:update --force
bin/console doctrine:fixtures:load
bin/console server:run
```
Enjoy http://127.0.0.1:8000