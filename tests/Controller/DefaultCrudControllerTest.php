<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 *
 * @package App\Tests\Controller
 */
class DefaultCrudControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $client = static ::createClient();
        $client->request('GET', '/article');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/create');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test',
            'article[description]' => 'fake article description',
            'article[created_at][date][month]' => '8',
            'article[created_at][date][day]' => '23',
            'article[created_at][date][year]' => '2019',
            'article[created_at][time][hour]' => '18',
            'article[created_at][time][minute]' => '4',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testEditAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/edit/1');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test edited',
            'article[description]' => 'fake article description edited',
            'article[created_at][date][month]' => '8',
            'article[created_at][date][day]' => '23',
            'article[created_at][date][year]' => '2019',
            'article[created_at][time][hour]' => '18',
            'article[created_at][time][minute]' => '4',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteArticle()
    {
        $client = static ::createClient();
        $client->request('GET', '/article/delete/1');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}