<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class DefaultControllerTest
 *
 * @package App\Tests\Controller
 */
class DefaultControllerTest extends WebTestCase
{
    public function testRegisterAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/register');
        $button = $crawler->selectButton('user_create[submit]');
        $form = $button->form([
            'user_create[username]' => 'user',
            'user_create[password][first]' => 'test123',
            'user_create[password][second]' => 'test123',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testLoginAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123'
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testIndexAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123'
        ]);
        $client->submit($form);

        $client->request('GET', '/article/ru');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testCreateAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123'
        ]);
        $client->submit($form);
        $crawler = $client->request('GET', '/article/ru/create');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test',
            'article[description]' => 'fake article description',
            'article[created_at][date][month]' => '8',
            'article[created_at][date][day]' => '23',
            'article[created_at][date][year]' => '2019',
            'article[created_at][time][hour]' => '18',
            'article[created_at][time][minute]' => '4',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testEditAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123'
        ]);
        $client->submit($form);
        $crawler = $client->request('GET', '/article/ru/edit/1');
        $button = $crawler->selectButton('article[submit]');
        $form = $button->form([
            'article[name]' => 'test edited',
            'article[description]' => 'fake article description edited',
            'article[created_at][date][month]' => '8',
            'article[created_at][date][day]' => '23',
            'article[created_at][date][year]' => '2019',
            'article[created_at][time][hour]' => '18',
            'article[created_at][time][minute]' => '4',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testDeleteArticle()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'user',
            'password' => 'test123'
        ]);
        $client->submit($form);
        $client->request('GET', '/article/ru/delete/1');
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}